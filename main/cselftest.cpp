//============================================================================
// Name        : cselftest.cpp
// Author      : Gibmir
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <algorithm>
#include <iostream>
#include <vector>

#include "lib/calculator.h"
#include "lib/io.h"

using namespace std;

int main()
{
  double x = readNumber();
  double y = readNumber();
  char operation = readOperation();
  double result = calculate(x, y, operation);
  writeResult(result);
  writeEven(isEven(result));
  return 0;
}
