/*
 * io.cpp
 *
 *  Created on: 27 дек. 2019 г.
 *      Author: ADMIN
 */
#include <iostream>
using namespace std;

double readNumber() {
  double result;
  std::cout << "Enter the number:" << endl;
  std::cin >> result;
  return static_cast<double>(result);
}
char readOperation() {
  char result;
  std::cout << "Enter the operation:" << endl;
  std::cin >> result;
  return result;
}
void writeResult(double result) { std::cout << "Result is:" << result << endl; }
void writeEven(bool isEven) {
  if (isEven) {
    std::cout << "Result is even" << std::endl;
  } else {
    std::cout << "Result is not even" << std::endl;
  }
}
