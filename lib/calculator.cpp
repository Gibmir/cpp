/*
 * calculator.cpp
 *
 *  Created on: 28 дек. 2019 г.
 *      Author: ADMIN
 */
#include "calculator.h"
#include "calculatorSymbols.h"
using namespace symbols;
using namespace functions;

double calculate(double x, double y, char operationSymbol) {
  if (operationSymbol == symbols::plus) {
    return functions::sum(x, y);
  } else if (operationSymbol == symbols::minus) {
    return functions::subtract(x, y);
  } else if (operationSymbol == symbols::multiply) {
    return functions::multiply(x, y);
  } else if (operationSymbol == symbols::division) {
    return functions::division(x, y);
  } else {
    throw 55;
  }
}

double functions::sum(double x, double y) { return x + y; }

double functions::subtract(double x, double y) { return x - y; }

double functions::multiply(double x, double y) { return x * y; }

double functions::division(double x, double y) { return x / y; }

bool isEven(int x) { return x % 2 == 0; }
