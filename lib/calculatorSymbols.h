#ifndef CALCULATOR_SYMBOLS_H
namespace symbols {
extern const char plus{'+'};
extern const char minus{'-'};
extern const char multiply{'*'};
extern const char division{'/'};
}  // namespace symbols
#endif  // !CALCULATOR_SYMBOLS_H
