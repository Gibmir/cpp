/*
 * io.h
 *
 *  Created on: 27 дек. 2019 г.
 *      Author: ADMIN
 */

#ifndef IO_H_
#define IO_H_
double readNumber();
void writeResult(double result);
void writeEven(bool isEven);
char readOperation();
#endif /* IO_H_ */
