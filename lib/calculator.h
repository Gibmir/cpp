/*
 * calculator.h
 *
 *  Created on: 28 дек. 2019 г.
 *      Author: ADMIN
 */

#ifndef CALCULATOR_H_
#define CALCULATOR_H_
namespace functions {
double sum(double x, double y);
double subtract(double x, double y);
double multiply(double x, double y);
double division(double x, double y);
}  // namespace functions
bool isEven(int x);
double calculate(double x, double y, char operationSymbol);
#endif /* CALCULATOR_H_ */
