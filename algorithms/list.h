#ifndef LIST_H_
#define LIST_H_
struct node
{
    int data;
    node *next;
};
class List
{
private:
    node *head;
    node *tail;

public:
    List();
    void operator+(int value);
    bool Contains(int value);
};
#endif
