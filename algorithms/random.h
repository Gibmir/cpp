#ifndef RANDOM_H_
#define RANDOM_H_
class Random
{
private:
    int *m_array;
    int array_length;
    int current;

public:
    Random(int a, int b, int m);
    ~Random();
    int next();
};
#endif