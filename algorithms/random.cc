#include "random.h"
#include "math.h"
#include <iostream>

inline int *generate(int a, int b, int m)
{
    int *p_array = new int[m];
    p_array[0] = 0;
    if (m > 1)
    {
        for (int i = 1; i < m; i++)
        {
            p_array[i] = (a * p_array[i - 1] + b) % m;
        }
    }
    return p_array;
}

Random::Random(int a, int b, int m)
{
    array_length = m;
    m_array = generate(a, b, array_length);
    current = 0;
}

Random::~Random()
{
    delete (m_array);
}
int Random::next()
{
    if (current == array_length)
    {
        current = 0;
    }
    return m_array[current++];
}
