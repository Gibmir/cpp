#include "list.h"
#include <iostream>
//List class implementation
List::List() : head(NULL), tail(NULL)
{
}

void List::operator+(int value)
{
    node *tmp = new node();
    tmp->data = value;
    tmp->next = NULL;
    if (head == NULL)
    {
        head = tmp;
        tail = tmp;
    }
    else
    {
        tail->next = tmp;
        tail = tail->next;
    }
}

bool List::Contains(int value)
{
    node *p_tmp = head;
    while (p_tmp != NULL)
    {
        if (p_tmp->data == value)
        {
            return true;
        }
        p_tmp = p_tmp->next;
    }
    return false;
}