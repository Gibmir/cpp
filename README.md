# cpp

Project is devoted to study algorithms and cpp.

---
## build & test

Project uses [bazel](https://bazel.build/) as a build system.

To build:
```
bazel build //main:cselftest
```

To test:
```
bazel test //test:unittests
```
