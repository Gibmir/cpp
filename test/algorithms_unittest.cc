#include "gtest/gtest.h"
#include "algorithms/random.h"

TEST(Numeric, NumericRandom)
{
    Random r(7, 5, 11);
    int expected[]{0, 5, 7, 10, 9, 2, 8, 6, 3, 4, 0};
    for (int i = 0; i < 11; i++)
    {
        EXPECT_EQ(expected[i], r.next());
    }
}