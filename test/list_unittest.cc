#include "gtest/gtest.h"
#include "algorithms/list.h"

TEST(Structures, LinkedList)
{
    List list = List();
    EXPECT_FALSE(list.Contains(0));
    list + 0;
    EXPECT_TRUE(list.Contains(0));
    EXPECT_FALSE(list.Contains(1));
    list + 1;
    EXPECT_TRUE(list.Contains(1));
}